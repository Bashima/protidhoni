package splgui.com.example.bashimaislam.protidhoni;

import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.widget.Toast;

public class MyPreferenceActivity extends PreferenceActivity {
    Preference.OnPreferenceChangeListener numberCheckListener = new Preference.OnPreferenceChangeListener() {
        @Override
        public boolean onPreferenceChange(Preference preference, Object newValue) {
            if (newValue != null && newValue.toString().length() > 0 && newValue.toString().matches("\\d*")) {
                return true;
            }
            Toast.makeText(MyPreferenceActivity.this, "Invalid Input", Toast.LENGTH_SHORT).show();
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        addPreferencesFromResource(R.xml.prefs);

        Preference soundPref = getPreferenceScreen().findPreference("soundLevel");

        soundPref.setOnPreferenceChangeListener(numberCheckListener);
    }
}
