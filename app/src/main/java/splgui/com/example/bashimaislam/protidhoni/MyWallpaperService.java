package splgui.com.example.bashimaislam.protidhoni;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Movie;
import android.graphics.Paint;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.service.wallpaper.WallpaperService;
import android.util.Log;
import android.view.SurfaceHolder;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;

public class MyWallpaperService extends WallpaperService {

    SplEngine mEngine = null;
    Context mContext = MyWallpaperService.this;

    Boolean mMode = true; // false -> fast , true -> slow
    Boolean mCalib = false;
    Boolean mLog = false;
    Boolean mMax = false;

    static final int MY_MSG = 1;

    public static double soundLevel = 20.00;


    @Override
    public Engine onCreateEngine() {
        try {
            String TAG = "MyWallpaperService";
            Log.v(TAG, "Creating Engine");
            return new RenderEngine();
        } catch (IOException e) {
            stopSelf();
            return null;
        }
    }

    public class RenderEngine extends Engine {
        private Movie img;
        private int mDuration;
        private final Handler handler = new Handler();
        private final Runnable drawRunner = new Runnable() {
            @Override
            public void run() {
                draw();
            }
        };
        private Paint paint = new Paint();
        private boolean visible = true;
        private float width;
        private float height;
        private float parentWidth;
        private float parentHeight;
        int curTime;
        long startTime;

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(MyWallpaperService.this);
        SharedPreferences.Editor editor = prefs.edit();

        RenderEngine() throws IOException {

            start_meter();
            InputStream is;

            editor.putString("soundLevel", "" + soundLevel);
            editor.commit();
//            Toast.makeText (getApplicationContext(),""+soundLevel+" in engine", Toast.LENGTH_SHORT ).show();
            if (soundLevel >= 95) {
                is = getResources().openRawResource(R.raw.confuesed);
            } else if (soundLevel >= 80) {
                is = getResources().openRawResource(R.raw.tiny);
            } else if (soundLevel >= 65) {
                is = getResources().openRawResource(R.raw.giphy);
            } else if (soundLevel >= 50) {
                is = getResources().openRawResource(R.raw.bird);
            } else {
                is = getResources().openRawResource(R.raw.nyan);
            }

            if (is != null) {
                try {
                    img = Movie.decodeStream(is);
                    mDuration = img.duration();
                } finally {
                    is.close();
                }
            } else {
                throw new IOException("Unable to open image");
            }
            paint.setAntiAlias(true);
            paint.setColor(Color.WHITE);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeJoin(Paint.Join.ROUND);
            paint.setStrokeWidth(10.0f);
            curTime = -1;
            handler.post(drawRunner);
        }

        @Override
        public void onVisibilityChanged(boolean visibility) {
            this.visible = visibility;
            if (visibility) {
                handler.post(drawRunner);
            } else {
                handler.removeCallbacks(drawRunner);
            }
        }

        @Override
        public void onSurfaceDestroyed(SurfaceHolder holder) {
            super.onSurfaceDestroyed(holder);
            this.visible = false;
            handler.removeCallbacks(drawRunner);
        }

        @Override
        public void onSurfaceChanged(SurfaceHolder holder, int format,
                                     int width, int height) {
            this.width = width / (1f * img.width());
            this.height = height / (1f * img.height());
            parentWidth = width;
            parentHeight = height;
            super.onSurfaceChanged(holder, format, width, height);
        }

        @Override
        public void onDestroy() {
            try {
                stop_meter();
            } catch (Exception e) {
                Log.d("Exception on onDestroy(): ", "e");
            }
            super.onDestroy();
            handler.removeCallbacks(drawRunner);
        }

        void checkSoundLevel() {
            InputStream is;

            editor.putString("soundLevel", "" + soundLevel);
            editor.commit();
//            Toast.makeText (getApplicationContext(),""+soundLevel+" in check", Toast.LENGTH_SHORT ).show();
            if (soundLevel >= 100) {
                is = getResources().openRawResource(R.raw.confuesed);
            } else if (soundLevel >= 85) {
                is = getResources().openRawResource(R.raw.tiny);
            } else if (soundLevel >= 70) {
                is = getResources().openRawResource(R.raw.giphy);
            } else if (soundLevel >= 55) {
                is = getResources().openRawResource(R.raw.bird);
            } else {
                is = getResources().openRawResource(R.raw.nyan);
            }

            img = Movie.decodeStream(is);
            mDuration = img.duration();
            curTime = -1;
        }

        void tick() {
            Log.d("tick","tick called");
            if (curTime == -1L) {
                curTime = 0;
                startTime = SystemClock.uptimeMillis();
            } else {
                long mDiff = SystemClock.uptimeMillis() - startTime;
                curTime = (int) (mDiff % mDuration);
            }

        }

        public void draw() {

            SurfaceHolder holder = getSurfaceHolder();
            try {
                double temp = Double.valueOf(prefs.getString("soundLevel", "null"));


                if (((soundLevel >= 100) && (temp >= 100)) || ((soundLevel >= 85) && (temp >= 85)) || ((soundLevel >= 70) && (temp >= 70)) || ((soundLevel >= 55) && (temp >= 55)) || ((soundLevel < 55) && (temp < 55))) {
                    checkSoundLevel();
                    width = parentWidth / (1f * img.width());
                    height = parentHeight / (1f * img.height());
                }
            }
            catch (Exception e)
            {
                Log.d("temp", "null");
            }
            tick();
            Canvas c = null;
            try {
                c = holder.lockCanvas();
                if (c != null) {
                    c.save();
                    c.scale(width, height);
                    img.setTime(curTime);
                    img.draw(c, 0, 0);
                    c.restore();
                }
            } finally {
                if (c != null) {
                    holder.unlockCanvasAndPost(c);
                }
            }
            handler.removeCallbacks(drawRunner);
            if (visible) {
                handler.postDelayed(drawRunner, 1000L / 100L);
            }
        }
    }

    public void start_meter() {
        mCalib = false;
        mMax = false;
        mLog = false;
        mMode = true;
        mEngine = new SplEngine(mhandle, mContext);
        mEngine.start_engine();
    }

    public void stop_meter() {
        mEngine.stop_engine();
    }

    public Handler mhandle = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MY_MSG:
                    soundLevel = Double.parseDouble(" " + msg.obj);

                    break;
                default:
                    super.handleMessage(msg);
                    break;
            }
        }

    };
}